package db

import (
	"testing"
	"time"
	"github.com/globalsign/mgo/bson"
)

// 插入设备
func TestAccessoryInsert(t *testing.T) {
	session, err := getSession()
	if err != nil {
		t.Fatal(err)
	}
	defer session.Close()

	acc := &Register{
		Id: "59e2c70da4f94b32",
		PrivateKey: "WeLHDaT5SzKjYSTD2kpNDQ==",
		HomeId: "2a6c06419d314abfb4bad2c6f3559924",
		Name: "HomeMasterV2",
		Model: "HM-02",
		Manufacturer: "https://adai.design",
		SerialNumber: "HM20180325004",
		FirmwareRevision: "0.1.0",
		Time: time.Now(),
	}

	collection := session.DB(dbName).C(collectionAccessory)
	// 设备是否已经存在
	if n, err := collection.Find(bson.M{"id": acc.Id}).Count(); err == nil && n != 0 {
		t.Fatalf("acc(%s) has already existed", acc.Id)
	}

	// 插入设备
	err = collection.Insert(acc)
	if err != nil {
		t.Fatal(err)
	}

	// 查找插入的设备
	var register *Register
	err = collection.Find(bson.M{"id": acc.Id}).One(&register)
	if err != nil {
		t.Fatal(err)
	}

	t.Log(register.String())
}

// 查找设备
func TestAccessoryFind(t *testing.T) {
	session, err := getSession()
	if err != nil {
		t.Fatal(err)
	}
	defer session.Close()

	accId := "59e2c70da4f94b32"

	// 查找插入的设备
	collection := session.DB(dbName).C(collectionAccessory)
	var register *Register
	err = collection.Find(bson.M{"id": accId}).One(&register)
	if err != nil {
		t.Fatal(err)
	}

	register.Date = register.Time.In(time.Local).Format("2006-01-02 15:04:05")
	t.Log(register.String())
}

// 插入设备
func TestAccessoryUpdate(t *testing.T) {
	session, err := getSession()
	if err != nil {
		t.Fatal(err)
	}
	defer session.Close()

	acc := &Register{
		Id: "8bd53509433b457b",
		PrivateKey: "i9U1CUM7RXuxUVHvSvHP1Q==",
		HomeId: "65904a3d-d5cd-47d7-9c3e-3bd05317cc53",
		Name: "HomeMasterV2",
		Model: "HM-02",
		Manufacturer: "https://adai.design",
		SerialNumber: "HM20180325004",
		FirmwareRevision: "0.1.0",
		Time: time.Now(),
	}

	collection := session.DB(dbName).C(collectionAccessory)
	// 设备是否已经存在
	if n, err := collection.Find(bson.M{"id": acc.Id}).Count(); err == nil && n == 0 {
		t.Fatalf("acc(%s) not exists", acc.Id)
	}

	// 插入设备
	err = collection.Update(bson.M{"id": acc.Id}, acc)
	if err != nil {
		t.Fatal(err)
	}

	// 查找插入的设备
	var register *Register
	err = collection.Find(bson.M{"id": acc.Id}).One(&register)
	if err != nil {
		t.Fatal(err)
	}

	register.Date = register.Time.In(time.Local).Format("2006-01-02 15:04:05")
	t.Log(register.String())
}

// 删除设备
func TestAccessoryRemove(t *testing.T) {
	session, err := getSession()
	if err != nil {
		t.Fatal(err)
	}
	defer session.Close()

	accId := "59e2c70da4f94b32"
	// 查找插入的设备
	collection := session.DB(dbName).C(collectionAccessory)
	info, err := collection.RemoveAll(bson.M{"id": accId})
	if err != nil {
		t.Fatal(err)
	}

	t.Logf("accessory remove(%d) match(%d)", info.Removed, info.Matched)
}

// 设备在线离线信息
func TestAccessoryStateInsert(t *testing.T) {
	session, err := getSession()
	if err != nil {
		t.Fatal(err)
	}
	defer session.Close()

	now, _ := time.Parse("2006-01-02 15:04:05", "2018-05-07 0:04:14")
	status := AccessoryStatus{
		Id: "59e2c70da4f94b32",
		State: AccessoryStateOnline,
		Time: now,
	}

	collection := session.DB(dbName).C(collectionAccessoryLog)
	// 设备是否已经存在

	for i:=0; i<10; i++ {
		// 插入设备
		err = collection.Insert(status)
		if err != nil {
			t.Fatal(err)
		}
		status.Time = status.Time.Add(time.Minute)
	}

	// 查找插入的设备
	var list []*AccessoryStatus
	err = collection.Find(bson.M{"id": status.Id}).All(&list)
	if err != nil {
		t.Fatal(err)
	}

	for _, st := range list {
		t.Log(st)
	}
}

func TestAccessoryStateFind(t *testing.T) {
	session, err := getSession()
	if err != nil {
		t.Fatal(err)
	}
	defer session.Close()

	accId := "59e2c70da4f94b32"
	after, _ := time.Parse("2006-01-02 15:04:05", "2018-05-07 0:12:14")

	collection := session.DB(dbName).C(collectionAccessoryLog)
	// 查找插入的设备
	var list []*AccessoryStatus
	err = collection.Find(bson.M{"id": accId, "time": bson.M{"$gte": after}}).All(&list)
	if err != nil {
		t.Fatal(err)
	}

	t.Logf("find count(%d)\n", len(list))
	for _, st := range list {
		t.Log(st)
	}
}


