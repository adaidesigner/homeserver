package db

import (
	"github.com/globalsign/mgo"
	"time"
	"github.com/globalsign/mgo/bson"
	"encoding/json"
)

func GetHomeSession() (*mgo.Session, *mgo.Collection) {
	session, err := getSession()
	if err != nil {
		return nil, nil
	}
	collection := session.DB(dbName).C(collectionHome)
	return session, collection
}

// 成员达到离开家庭日志记录
type HomeMemberState struct {
	HomeId 		string		`json:"-" bson:"home"`
	MemberId 	string		`json:"member" bson:"member"`
	State 		int			`json:"state" bson:"state"`
	Time 		time.Time	`json:"-" bson:"time"`
	Date 		string		`json:"time" bson:"-"`
}

// 家庭成员位置记录
func HomeMemberMark(state *HomeMemberState) error {
	session, err := getSession()
	if err != nil {
		return err
	}
	defer session.Close()

	collection := session.DB(dbName).C(collectionHomeMember)
	err = collection.Insert(state)
	if err != nil {
		return err
	}
	return nil
}

func HomeMemberLocationInfoGet(home string, max int) []*HomeMemberState {
	session, err := getSession()
	if err != nil {
		return nil
	}
	defer session.Close()
	collection := session.DB(dbName).C(collectionHomeMember)

	var list []*HomeMemberState
	filter := bson.M{
		"home": home,
	}
	err = collection.Find(filter).Limit(max).All(&list)
	if err != nil {
		return nil
	}
	return list
}

type HomePushInfo struct {
	HomeId 	string		`json:"home" bson:"home"`
	Body 	string		`json:"body" bson:"body"`
	Time 	time.Time	`json:"-" bson:"time"`
	Date 	string		`json:"date" bson:"-"`
}

func (h *HomePushInfo) String() string {
	h.Date = h.Time.Format("2006-01-02 15:04:05")
	buf, _ := json.Marshal(h)
	return string(buf)
}

// 家庭成员位置记录
func HomePushMark(info *HomePushInfo) error {
	session, err := getSession()
	if err != nil {
		return err
	}
	defer session.Close()

	collection := session.DB(dbName).C(collectionHomePush)
	err = collection.Insert(info)
	if err != nil {
		return err
	}
	return nil
}

func HomePushInfoGet(home string, max int) []*HomePushInfo {
	session, err := getSession()
	if err != nil {
		return nil
	}
	defer session.Close()
	collection := session.DB(dbName).C(collectionHomePush)

	var list []*HomePushInfo
	filter := bson.M{
		"home": home,
	}
	err = collection.Find(filter).Limit(max).All(&list)
	if err != nil {
		return nil
	}
	return list
}


type HomeCharacteristicInfo struct {
	HomeId 			string		`json:"home" bson:"home"`
	AccessoryId 	string		`json:"aid" bson:"aid"`
	ServiceId 			int 	`json:"sid" bson:"sid"`
	CharacteristicId 	int 	`json:"cid" bson:"cid"`
	Value 			interface{}	`json:"value" bson:"value"`
	Time 			time.Time	`json:"-" bson:"time"`
	Date 			string		`json:"time" bson:"-"`
}

func HomeCharacteristicMark(info *HomeCharacteristicInfo) error {
	session, err := getSession()
	if err != nil {
		return err
	}
	defer session.Close()

	collection := session.DB(dbName).C(collectionHomeCharacteristic)
	err = collection.Insert(info)
	if err != nil {
		return err
	}
	return nil
}

type HomeActionInfo struct {
	HomeId 			string		`json:"home" bson:"home"`
	AccessoryId 	string		`json:"aid" bson:"aid"`
	ServiceId 			int 	`json:"sid" bson:"sid"`
	CharacteristicId 	int 	`json:"cid" bson:"cid"`
	Value 			interface{}	`json:"value" bson:"value"`
	Time 			time.Time	`json:"-" bson:"time"`
}

func HomeActionsMark(action *HomeActionInfo) error {
	session, err := getSession()
	if err != nil {
		return err
	}
	defer session.Close()

	collection := session.DB(dbName).C(collectionHomeAction)
	err = collection.Insert(action)
	if err != nil {
		return err
	}
	return nil
}

// 属性查找顾虑器
type CharacteristicFilter struct {
	Home	string		`json:"home" bson:"home"`
	Aid 	string		`json:"aid" bson:"aid"`
	Sid 	int			`json:"sid" bson:"sid"`
	Cid 	int 		`json:"cid" bson:"cid"`
	After 	time.Time 	`json:"-" bson:"after,omitempty"`
	Time 	string		`json:"after" bson:"-"`
	Max 	int 		`json:"max,omitempty" bson:"max,omitempty"`
}



func HomeCharacteristicInfoGet(filter *CharacteristicFilter) []*HomeCharacteristicInfo {
	session, err := getSession()
	if err != nil {
		return nil
	}
	defer session.Close()
	collection := session.DB(dbName).C(collectionHomeCharacteristic)

	query := bson.M{
		"home": filter.Home,
		"aid": filter.Aid,
		"sid": filter.Sid,
		"cid": filter.Cid,
		"time": bson.M{
			"$gte": filter.After,
		},
	}

	max := 0
	if filter.Max == 0 {
		max = 10000
	}

	var list []*HomeCharacteristicInfo
	err = collection.Find(query).Limit(max).All(&list)
	if err != nil {
		return  nil
	}
	return list
}





