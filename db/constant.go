package db

const (
	dbName = "home"
	collectionAccessory = "accessory"
	collectionAccessoryLog = "accessory-log"
	collectionMembership = "membership"
	collectionMembershipLog = "membership-log"
	collectionConnInvalid = "conn-invalid"
	collectionHome = "home"
	collectionHomeMember = "home-member"
	collectionHomePush = "home-push"
	collectionHomeCharacteristic = "home-characteristic"
	collectionHomeAction = "home-action"
)



