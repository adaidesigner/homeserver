package db

import (
	"testing"
	"github.com/globalsign/mgo/bson"
	"time"
	"os"
	"strconv"
	"fmt"
)

func TestTemperatureData(t *testing.T) {
	session, err := getSession()
	if err != nil {
		t.Fatal(err)
	}
	defer session.Close()

	collection := session.DB(dbName).C(collectionHomeCharacteristic)

	var list []*HomeCharacteristicInfo
	filter := bson.M{"aid": "a4c2532b877846e8", "sid": 1, "cid": 1}
	err = collection.Find(filter).All(&list)
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("len(%d)", len(list))

	for _, cell := range list {
		cell.Date = cell.Time.In(time.Local).Format("2006-01-02 15:04:05")
	}

	perm, _ := strconv.ParseInt("0666", 8, 64)
	file, err := os.OpenFile("/Users/yun/Desktop/ht.log", os.O_CREATE|os.O_WRONLY, os.FileMode(perm))
	if err != nil {
		t.Fatal(err)
	}
	defer file.Close()

	var buf string
	for _, cell := range list {
		if v := cell.Value.(float64); v > 100 {
			continue
		}

		str := fmt.Sprintf("%s\t %v\n", cell.Date, cell.Value)
		buf += str
		fmt.Printf(str)
	}
	file.WriteString(buf)

}


