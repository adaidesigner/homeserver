package db

import (
	"testing"
	"encoding/json"
	"time"
	"fmt"
	"strconv"
	"os"
)

type pushInfo struct {
	Time 	string		`json:"time"`
	Title  	string		`json:"title"`
	Body 	string		`json:"body"`
}

func TestHomePushInfoGet(t *testing.T) {
	home := "65904a3d-d5cd-47d7-9c3e-3bd05317cc53"
	list := HomePushInfoGet(home, 100)
	if list == nil {
		t.Fatal("find err")
	}

	var infos []*pushInfo
	for _, cell := range list {
		var info pushInfo
		err := json.Unmarshal([]byte(cell.Body), &info)
		if err != nil {
			continue
		}
		info.Time = cell.Time.Format("2006-01-02 15:04:05")
		infos = append(infos, &info)
	}

	buf, _ := json.Marshal(infos)
	t.Log(string(buf))
}

func TestHomeCharacteristicInfoGet(t *testing.T) {
	filter := CharacteristicFilter{
		Home: "65904a3d-d5cd-47d7-9c3e-3bd05317cc53",
		Aid: "a4c2532b877846e8",
		Sid: 1,
		Cid: 1,
		After: time.Now().Add(-time.Hour * 24),
		Max: 10000,
	}

	t.Logf("time now: %s", filter.After.Format("2006-01-02 15:04:05"))

	list := HomeCharacteristicInfoGet(&filter)
	if list == nil {
		t.Fatal("mongodb find err")
	}

	t.Logf("marks count(%d)", len(list))
	for _, cell := range list {
		cell.Date = cell.Time.In(time.Local).Format("2006-01-02 15:04:05")
	}

	perm, _ := strconv.ParseInt("0666", 8, 64)
	file, err := os.OpenFile("/Users/yun/Desktop/ht.log", os.O_CREATE|os.O_WRONLY, os.FileMode(perm))
	if err != nil {
		t.Fatal(err)
	}
	defer file.Close()

	var buf string
	for _, cell := range list {
		if v := cell.Value.(float64); v > 100 || v < 10{
			continue
		}
		str := fmt.Sprintf("%s\t %v\n", cell.Date, cell.Value)
		buf += str
		fmt.Printf(str)
	}
	file.WriteString(buf)
}
