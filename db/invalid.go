package db

import "time"

// 非法的、无效的连接
// 指登陆认证失败的链接

const (
	InvalidTcp = "tcp"
	InvalidWebsocket = "websocket"
	InvalidHttps = "https"
)

type InvalidInfo struct {
	Addr 	string		`json:"addr" bson:"addr"`
	Type 	string		`json:"type" bson:"type"`
	Time 	time.Time	`json:"time" bson:"time"`
}

func Invalid(info *InvalidInfo) error {
	session, err := getSession()
	if err != nil {
		return err
	}
	defer session.Close()
	collection := session.DB(dbName).C(collectionConnInvalid)
	return collection.Insert(info)
}