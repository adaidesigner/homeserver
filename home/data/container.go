package data

type Characteristic struct {
	AId   string      `json:"aid"`
	SId   int         `json:"sid"`
	CId   int         `json:"cid"`
	Value interface{} `json:"value"`
}

type ContainerCharacteristic struct {
	Id 			string		`json:"container"`
	IsReachable bool		`json:"reachable"`
	Version 	int 		`json:"version,omitempty"`
	Chars 		[]*Characteristic	`json:"characteristics,omitempty"`
}


