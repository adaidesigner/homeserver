package home

import (
	"adai.design/homeserver/members"
	"encoding/json"
)

type Room struct {
	Id 		string		`json:"id" bson:"id"`
	Name 	string		`json:"name" bson:"name"`
}

type RoomManager struct {
	rooms 	[]*Room
}

func (rm *RoomManager) Handle(h *Home, pkg *members.MessagePkg) error {
	msg := pkg.Msg
	if msg.Method == members.MsgMethodGet {
		buf, _ := json.Marshal(rm.rooms)
		ack := &members.MessagePkg{
			Type: pkg.Type,
			Ctx: pkg.Ctx,
			Msg: &members.Message{
				Path: msg.Path,
				Method: msg.Method,
				State: "ok",
				Home: h.Id,
				Data: buf,
			},
		}
		h.replyMemberResult(ack)
	}
	return nil
}