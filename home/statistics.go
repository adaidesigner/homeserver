package home

import (
	"adai.design/homeserver/members"
	"adai.design/homeserver/db"
	"encoding/json"
	"adai.design/homeserver/log"
	"time"
)

type statistics struct {}

type pushInfo struct {
	Time 	string		`json:"time"`
	Title  	string		`json:"title"`
	Body 	string		`json:"body"`
}

// 推送消息记录获取
func (s *statistics) handlePushInfo(home *Home, pkg *members.MessagePkg) error {
	msg := pkg.Msg
	list := db.HomePushInfoGet(home.Id, 50)
	if list == nil {
		return nil
	}

	var infos []*pushInfo
	for _, cell := range list {
		var info pushInfo
		err := json.Unmarshal([]byte(cell.Body), &info)
		if err != nil {
			continue
		}
		info.Time = cell.Time.In(time.Local).Format("2006-01-02 15:04:05")
		infos = append(infos, &info)
	}

	buf, _ := json.Marshal(infos)
	ack := &members.MessagePkg{
		Type: pkg.Type,
		Ctx: pkg.Ctx,
		Msg: &members.Message{
			Path: msg.Path,
			Method: msg.Method,
			State: "ok",
			Home: home.Id,
			Data: buf,
		},
	}
	home.replyMemberResult(ack)
	return nil
}

// 位置消息记录
func (s *statistics) handleLocationInfo(home *Home, pkg *members.MessagePkg) error {
	msg := pkg.Msg
	list := db.HomeMemberLocationInfoGet(home.Id, 50)
	if list == nil {
		return nil
	}

	var infos []*db.HomeMemberState
	for _, cell := range list {
		var info db.HomeMemberState
		info.MemberId = cell.MemberId
		info.State = cell.State
		info.Date = cell.Time.In(time.Local).Format("2006-01-02 15:04:05")
		infos = append(infos, &info)

	}

	buf, _ := json.Marshal(infos)
	ack := &members.MessagePkg{
		Type: pkg.Type,
		Ctx: pkg.Ctx,
		Msg: &members.Message{
			Path: msg.Path,
			Method: msg.Method,
			State: "ok",
			Home: home.Id,
			Data: buf,
		},
	}
	home.replyMemberResult(ack)
	return nil
}

type CharacteristicInfoValue struct {
	Time 	string			`json:"time"`
	Value 	interface{}		`json:"value"`
}

type CharacteristicInfos struct {
	Aid 	string		`json:"aid"`
	Sid 	int			`json:"sid"`
	Cid 	int			`json:"cid"`
	Changes 	[]*CharacteristicInfoValue		`json:"changes"`
}


func (s *statistics) handleCharacteristicInfo(home *Home, pkg *members.MessagePkg) error {
	msg := pkg.Msg
	var filter db.CharacteristicFilter
	err := json.Unmarshal(msg.Data, &filter)
	if err != nil {
		log.Error("json err: %s", err)
		return err
	}
	filter.Home = home.Id
	filter.After, err = time.ParseInLocation("2006-01-02 15:04:05", filter.Time, time.Local)
	if err != nil {
		log.Error("time format err: %s", err)
		return err
	}

	chars := db.HomeCharacteristicInfoGet(&filter)
	changes := make([]*CharacteristicInfoValue, len(chars))
	for i, cell := range chars {
		changes[i] = &CharacteristicInfoValue{
			Value: cell.Value,
			Time: cell.Time.In(time.Local).Format("2006-01-02 15:04:05"),
		}
	}
	info := &CharacteristicInfos {
		Aid: filter.Aid,
		Sid: filter.Sid,
		Cid: filter.Cid,
		Changes: changes[:],
	}
	buf, _ := json.Marshal(info)
	ack := &members.MessagePkg{
		Type: pkg.Type,
		Ctx: pkg.Ctx,
		Msg: &members.Message{
			Path: msg.Path,
			Method: msg.Method,
			State: "ok",
			Home: home.Id,
			Data: buf,
		},
	}
	home.replyMemberResult(ack)
	return nil
}

func (s *statistics) Handle(home *Home, pkg *members.MessagePkg) error {

	switch pkg.Msg.Path {
	case PathStatisticPush:
		return s.handlePushInfo(home, pkg)

	case PathStatisticLocation:
		return s.handleLocationInfo(home, pkg)

	case PathStatisticCharacteristic:
		return s.handleCharacteristicInfo(home, pkg)

	default:
		log.Error("unknown path(%s)", pkg.Msg.Path)
		return nil
	}
	return nil
}



