package home

import (
	"testing"
	"github.com/gorilla/websocket"
	"time"
	"encoding/json"
	"adai.design/homeserver/members"
	"adai.design/homeserver/db"
)

func TestLogin(t *testing.T) {
	c, _, err := websocket.DefaultDialer.Dial("wss://adai.design:6667/membership", nil)
	if err != nil {
		t.Fatal(err)
	}
	defer c.Close()

	data := map[string]interface{}{
		"account": "18565381403",
		"password": "123456",
		"date": time.Now().Format("2006-01-02 15:04:05"),
		"dev_t": "ios",
	}

	buf, _ := json.Marshal(data)

	msg := &members.Message{
		Path: "login",
		Method: members.MsgMethodPost,
		Data: buf,
	}
	buf, _ = json.Marshal(msg)
	c.WriteMessage(websocket.BinaryMessage, buf)

	_, buf, err = c.ReadMessage()
	if err != nil {
		t.Fatal(err)
	}
	t.Log("result: ", string(buf))
}

func apiSendRequest(msg *members.Message) ([]byte, error) {
	c, _, err := websocket.DefaultDialer.Dial("wss://adai.design:6667/membership", nil)
	if err != nil {
		return nil, err
	}
	defer c.Close()

	data := map[string]interface{}{
		"account": "18565381403",
		"password": "123456",
		"date": time.Now().Format("2006-01-02 15:04:05"),
		"dev_t": "macbook",
	}

	buf, _ := json.Marshal(data)

	login := &members.Message{
		Path: "login",
		Method: members.MsgMethodPost,
		Data: buf,
	}
	buf, _ = json.Marshal(login)
	c.WriteMessage(websocket.BinaryMessage, buf)

	_, buf, err = c.ReadMessage()
	if err != nil {
		return nil, err
	}

	msg.Home = "65904a3d-d5cd-47d7-9c3e-3bd05317cc53"
	buf, _ = json.Marshal(msg)
	c.WriteMessage(websocket.BinaryMessage, buf)

	_, buf, err = c.ReadMessage()
	if err != nil {
		return nil, err
	}
	return buf, err
}

func TestPathHome(t *testing.T) {
	msg := &members.Message{
		Path: PathHome,
		Method: members.MsgMethodGet,
	}
	result, err := apiSendRequest(msg)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(string(result))
}

func TestPathContainer(t *testing.T) {
	msg := &members.Message{
		Path: PathContainer,
		Method: members.MsgMethodGet,
	}
	result, err := apiSendRequest(msg)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(string(result))
}

func TestPathContainerCharacteristic(t *testing.T) {
	msg := &members.Message{
		Path: PathContainerCharacteristic,
		Method: members.MsgMethodGet,
	}
	result, err := apiSendRequest(msg)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(string(result))
}

func TestPathCharacteristic(t *testing.T)  {
	actions := []*Action{
		{
			AId:   "63b14730",
			SId:   1,
			CId:   1,
			Value: 0,
		}, /* {
			AId:   "aa9f362b",
			SId:   1,
			CId:   1,
			Value: 1,
		}, {
			AId:   "aa9f362b",
			SId:   2,
			CId:   1,
			Value: 1,
		},*/
	}
	buf, _ := json.Marshal(actions)
	msg := &members.Message{
		Path: PathCharacteristic,
		Method: members.MsgMethodPost,
		Data: buf,
	}
	result, err := apiSendRequest(msg)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(string(result))
}

func TestPathCharacteristicAir(t *testing.T)  {
	actions := []*Action{
		{
			AId:   "8bd53509433b457b",
			SId:   101,
			CId:   1,
			Value: 0,
		},
	}
	buf, _ := json.Marshal(actions)
	msg := &members.Message{
		Path: PathCharacteristic,
		Method: members.MsgMethodPost,
		Data: buf,
	}
	result, err := apiSendRequest(msg)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(string(result))
}


func TestPathService(t *testing.T) {
	msg := &members.Message{
		Path: PathService,
		Method: members.MsgMethodGet,
	}
	result, err := apiSendRequest(msg)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(string(result))
}

func TestPathScene(t *testing.T) {
	msg := &members.Message{
		Path: PathScene,
		Method: members.MsgMethodGet,
	}
	result, err := apiSendRequest(msg)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(string(result))
}

func TestPathScenePost(t *testing.T) {
	post := &ScenePost{
		Type: scenePostExecute,
		Id: "43ddede5-7a9f-4b7d-878f-d4be3e349b61",
	}
	buf, _ := json.Marshal(post)
	msg := &members.Message{
		Path: PathScene,
		Method: members.MsgMethodPost,
		Data: buf,
	}
	result, err := apiSendRequest(msg)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(string(result))
}

func TestPathRoom(t *testing.T) {
	msg := &members.Message{
		Path: PathRoom,
		Method: members.MsgMethodGet,
	}
	result, err := apiSendRequest(msg)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(string(result))
}

func TestPathMember(t *testing.T) {
	msg := &members.Message{
		Path: PathMember,
		Method: members.MsgMethodGet,
	}
	result, err := apiSendRequest(msg)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(string(result))
}

func TestPathAutomation(t *testing.T) {
	msg := &members.Message{
		Path: PathAutomation,
		Method: members.MsgMethodGet,
	}
	result, err := apiSendRequest(msg)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(string(result))
}

func TestPathAutomationPost(t *testing.T) {
	post := &AutomationPost{
		Type: autoPostExecute,
		Id: "d65b3c6b-ac22-4d78-9532-907aac7e5ece",
	}
	buf, _ := json.Marshal(post)
	msg := &members.Message{
		Path: PathAutomation,
		Method: members.MsgMethodPost,
		Data: buf,
	}
	result, err := apiSendRequest(msg)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(string(result))
}

func TestPathStatisticPush(t *testing.T) {
	msg := &members.Message{
		Path: PathStatisticPush,
		Method: members.MsgMethodGet,
	}
	result, err := apiSendRequest(msg)
	if err != nil {
		t.Fatal(err)
	}
	//t.Log(string(result))

	err = json.Unmarshal(result, &msg)
	if err != nil {
		t.Fatal(err)
	}
	var info []*pushInfo
	err = json.Unmarshal(msg.Data, &info)
	if err != nil {
		t.Fatal(err)
	}
	for _, cell := range info {
		t.Logf("%s  %s \t %s\n", cell.Time, cell.Title, cell.Body)
	}
}

func TestPathStatisticLocation(t *testing.T) {
	msg := &members.Message{
		Path: PathStatisticLocation,
		Method: members.MsgMethodGet,
	}
	result, err := apiSendRequest(msg)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(string(result))

	err = json.Unmarshal(result, &msg)
	if err != nil {
		t.Fatal(err)
	}
	var info []*db.HomeMemberState
	err = json.Unmarshal(msg.Data, &info)
	if err != nil {
		t.Fatal(err)
	}
	for _, cell := range info {
		t.Logf("%s  %s \t %d\n", cell.Date, cell.MemberId, cell.State)
	}
}


func TestPathStatisticCharacteristic(t *testing.T) {
	filter := &db.CharacteristicFilter{
		Aid: "a4c2532b877846e8",
		Sid: 1,
		Cid: 2,
		Time: time.Now().Add(- time.Hour * 24).Format("2006-01-02 15:04:05"),
	}
	buf, _ := json.Marshal(filter)
	msg := &members.Message{
		Path: PathStatisticCharacteristic,
		Method: members.MsgMethodGet,
		Data: buf,
	}
	result, err := apiSendRequest(msg)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(string(result))
}

func TestTimeParse(t *testing.T) {
	after, _ := time.ParseInLocation("2006-01-02 15:04:05", "2018-06-12 22:52:14", time.Local)
	t.Logf("%v", after)
}