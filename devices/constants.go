package devices

import "time"

const (
	idleTimeout = time.Second * 16
	verifyTimeout = time.Second * 5
)
