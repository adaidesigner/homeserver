package devices

var deviceMessageHandleMap = map[string]Handler{}

type Handler interface {
	handle(w *Reception, msg *Message) error
}

type HeartbeatController struct {}

func (h *HeartbeatController) handle(w *Reception, msg *Message) error {
	ack := &Message{
		Path:  msgPathHeartbeat,
		State: "ok",
	}
	w.writeMessage(ack)
	return nil
}

func init() {
	deviceMessageHandleMap[msgPathHeartbeat] = &HeartbeatController{}
}

