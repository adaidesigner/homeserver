package members

import (
	"github.com/astaxie/beego/httplib"
	"encoding/json"
	"io/ioutil"
	"testing"
)

func request(msg interface{}) ([]byte, error) {
	request := httplib.Post("https://adai.design:6667/location")

	data, _ := json.Marshal(msg)
	request.Body(data)

	res, err := request.DoRequest()
	if err != nil {
		return nil, err
	}

	data, err = ioutil.ReadAll(res.Body)
	res.Body.Close()

	return data, nil
}

func TestLocationLeave(t *testing.T) {
	data := map[string]interface{} {
		"home":"65904a3d-d5cd-47d7-9c3e-3bd05317cc53",
		"state":"leave",
		"session":"5a4c2089bfce4ebe15164d3ce44744d5",
		"membership":"34c92c64-6d84-490f-8e25-e27e1c8bf58e",
	}

	ack, err := request(data)
	if err != nil {
		t.Fatal(err)
	}

	t.Log(string(ack))
}


func TestLocationArrive(t *testing.T) {
	data := map[string]interface{} {
		"home":"65904a3d-d5cd-47d7-9c3e-3bd05317cc53",
		"state":"arrive",
		"session":"5a4c2089bfce4ebe15164d3ce44744d5",
		"membership":"34c92c64-6d84-490f-8e25-e27e1c8bf58e",
	}

	ack, err := request(data)
	if err != nil {
		t.Fatal(err)
	}

	t.Log(string(ack))
}