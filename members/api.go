package members

import "net/http"

type api struct {}

func (*api) ServeHTTP(response http.ResponseWriter, request *http.Request) {
	response.Write([]byte("let's go!"))
}

func init() {
	router["api"] = &api{}
}