package members

var memberMessageRouter = map[string]handler{}

type handler interface {
	handle(w *reception, msg *Message) error
}

type heartbeatController struct {}

func (h *heartbeatController) handle(w *reception, msg *Message) error {
	ack := &Message{
		Path: msgPathHeartbeat,
		State: "ok",
	}
	w.writeMessage(ack)
	return nil
}

func init() {
	memberMessageRouter[msgPathHeartbeat] = &heartbeatController{}
}


