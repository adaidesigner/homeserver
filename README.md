# HomeServer
智能家居DIY项目服务端部分 ([https://adai.design](https://adai.design))

## 智能家居DIY项目 v2018版
----
该工程属于属于智能家居DIY项目的一部分，该项目主要包括:
- [**Home** iOS应用程序，由Swift编写，负责家居设备、场景、自动化的查看与设置](https://gitee.com/adaidesigner/home)
- [**HomeMaster** 家电控制网关(c+golang)，负责设备传感器的控制与查询](https://gitee.com/adaidesigner/homemaster)
- **HomeServer** 服务器应用程序(golang)，负责设备、手机端接入，以及家居场景自动化功能
- [**HomeMaster-Driver** 硬件设备驱动，由C语言编写的内核模块](https://gitee.com/adaidesigner/homemaster-driver)

## 智能家居DIY项目结构
![](conf/architecture.png)

- **Master**: 网关终端负责设备状态的查询反馈、控制、以及从属设备的状态检查。
- **Home**: 家庭手机App提供人家交互界面，供用户使用设备场景以及设置自动化
- **Server**: 云服务器负责将设备和用户连接起来，传递控制信息，执行自动化功能。
- (注: 境外服务器走https可能会受到干扰，当情况发生时，应该多准备梯子)

## HomeServer项目结构
![](conf/home.png)

服务器项目主要分为三大块，设备、家庭、注册用户，自动化属于家庭的一部分。



